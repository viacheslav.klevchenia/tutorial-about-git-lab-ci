# DevOps with GitLab CI Course - Build Pipelines and Deploy to AWS

👋 Welcome to this GitLab CI course available on freeCodeCamp.

## Original Getting started notes (from author)

- **Check the** [**course notes**](docs/course-notes.md)
- Watch the [GitLab CI course on freecodecamp](https://www.youtube.com/watch?v=PGyhBwLyK2U)

- Something is wrong? [**Submit an issue.**](https://gitlab.com/gitlab-course-public/freecodecamp-gitlab-ci/-/issues/new?issue%5Bmilestone_id%5D=)

## Slava's notes

1. It was a problem when used wrong ngnix version in Doeckerfile, so looks like ngnix 1.12 and 1.20 are very different!
2. `rules` part helps to run some steps only if some conditions are met
3. `extends` section will run predefined step. Dot in the name are not mandatory
4. step name isn't important to CI, just for display, but steps with dot at start will be skipped from run
5. order of stages/steps are according to stages list. Steps from same stage will be run in parallel.
6. variables can be in scope of step, pipeline, GitLab (`$CI_PIPELINE_IID`)
7. AWS resources (S3 buckets, ElasticBeansTalk instance etc.) should be defined before. As well user to access and permissions (please check video in case of troubles)
8. Remember about static website sharing/access-rights-to resources
9. Some scripts just-need-to-be-known-from-documentation (like `export DEPLOY_TOKEN=$(echo $GITLAB_DEPLOY_TOKEN | tr -d "\n" | base64)`)
10. `aws s3 sync build s3://$AWS_S3_BUCKET --delete` will sync files between source and s3. Without `--delete` old files will not be deleted
11. After `envsubst < templates/auth.json > auth.json` file from template will be added to environment AND populated with actual values of variables
12. `aws elasticbeanstalk wait environment-updated` smart-sleep/wait out-of-box
13. `.gitlab-ci.2.yml` was taken from another (simplier) project, but here SAST point included:
```yml
sast:
  stage: test

include:
- template: Security/SAST.gitlab-ci.yml
```

